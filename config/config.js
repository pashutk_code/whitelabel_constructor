var path = require('path');

var templates = {
  config: path.join(__dirname, '..', 'templates', 'config.jade'),
  headerojst: path.join(__dirname, '..', 'templates', 'header.jade'),
  headerscss: path.join(__dirname, '..', 'templates', 'headerscss.jade'),
  footerojst: path.join(__dirname, '..', 'templates', 'footer.jade'),
  footerscss: path.join(__dirname, '..', 'templates', 'footerscss.jade'),
  basejs: path.join(__dirname, '..', 'templates', 'basejs.jade'),
  styles: path.join(__dirname, '..', 'templates', 'indexcss.jade')
};

var regexp = {
  asseturl: /(url)\(([\w\.\/\-\?\#]*)\){1}?/g,
  cssUrl: /url\(['"]?(.+?)['"]?\)/,
  datauri: /^['"]?data:/,
  imageSrc: /src=['"](.+?)['"]/,
  iframe: /(<iframe[\S\s]*?<\/iframe>)/g
};

/**
 * default settings from owl config with desc
 *
 * @type {Array}
 */
var moduleSettings = [
  {
    'name': 'extra_field_is_price',
    'value': true,
    'description': 'Amount sell b2b2c'
  }, {
    'name': 'owl_auth_required',
    'value': false,
    'description': ''
  }, {
    'name': 'owl_welcome_title',
    'value': '',
    'description': 'Browser tab text'
  }, {
    'name': 'owl_fill_names',
    'value': true,
    'description': ''
  }, {
    'name': 'owl_show_extra_field',
    'value': false,
    'description': 'Is need to show extra field'
  }, {
    'name': 'owl_show_banners',
    'value': false,
    'description': 'Enable banners'
  }, {
    'name': 'owl_show_chat',
    'value': false,
    'description': ''
  }, {
    'name': 'owl_show_account_block',
    'value': false,
    'description': 'Show account block in header'
  }, {
    'name': 'owl_searchform_title',
    'value': '',
    'description': 'Heading of the search form'
  }, {
    'name': 'owl_show_destinations',
    'value': false,
    'description': 'Enable popular destinations'
  }, {
    'name': 'owl_support_site',
    'value': '',
    'description': 'Support site name in voucher\'s footer'
  }, {
    'name': 'user_can_view_subscriptions',
    'value': false,
    'description': ''
  }, {
    'name': 'owl_anon_voucher',
    'value': false,
    'description': 'Hide ostrovok mentions in voucher'
  }, {
    'name': 'phones',
    'value': '',
    'description': 'Separate by comma',
    'placeholder': 'Example: \'+71234567890, 8 (495) 1234567\''
  }
];

module.exports = {
  moduleSettings: moduleSettings,
  regexp: regexp,
  templates: templates
};
