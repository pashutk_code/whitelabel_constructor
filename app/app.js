"use strict";

var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var fs = require('fs');
var methods = require('./methods');
var path = require('path');
var zlib = require('zlib');
var archiver = require('archiver');
var favicon = require('express-favicon');
var jade = require('jade');
var config = require('../config/config');
var getArchive = require('./parser');

var app = express();
app.use(favicon(path.join(__dirname, '..', 'public', 'favicon.ico')));

app.set('title', 'le constructeur de Whitelabel');
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, '..', 'public')));
app.set('view engine', 'jade');
app.set('views', path.join(__dirname, '..', 'views'));

app.get('/', function(req, res) {
  res.render('index', {
    moduleSettings: config.moduleSettings
  });
});

app.post('/', function(req, res) {
  var body = req.body;
  getArchive(req.body).then(data => {
    data.pipe(res);
    data.finalize();
    res.attachment(body.slug +'.zip');
  }).catch(err => {
    res.end(err);
  });
});

app.get('/inliner', function(req, res) {
 return res.render('inliner');
});

app.post('/inliner', function(req, res) {
  var html = req.body.html;
  var css = req.body.css;
  return uncss(html, {raw: css}, function(error, results) {
    var output = results;
    return res.render('res', {
      message: '3ANH/\\ANHN/\\',
      out: output
    })
  })
});

app.get('/getpics', function(req, res) {
  return res.render('getpics');
});

app.post('/getpics', function(req, res) {
  var url = 'http://www.google.com/images/errors/robot.png';

  var archive = archiver('zip');
  archive.on('error', function(err) {
    return res.status(500).send({error: err.message})
  });

  res.on('close', function() {
    return res.status(200).send('OK').end();
  });

  res.attachment('archive-name.zip');

  archive.pipe(res);

  var files = [url, 'http://www.google.com/images/errors/logo_sm_2.png'];

  for (var file in files) {
    archive.append(request(file), { name: path.basename(file) });
  }

  return archive.finalize();
});

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

//var port = process.env.NODE_ENV === 'production' ? 80: 3000;

var server = app.listen(1777, function() {
  var host = server.address().address;
  var port = server.address().port;

  console.log(`Server running on ${host}${port}`);
});
