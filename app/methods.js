var config = require('../config/config');
var XRegExp = require('xregexp').XRegExp;
var url = require('url');
var path = require('path');
var request = require('request');

var handleLink = function(href, request) {
  var httppos = href.search('http');
  var output;
  if (httppos === 0) {
    output = href;
  } else {
    if (href[0] === '/') {
      if (href[1] === '/') {
        output = 'http:' + href;
      } else {
        var pref = request.uri.protocol + '//' +request.host;
        output = pref + href;
      }
    } else {
      output = request.uri.href + href;
    }
  }
  return output
};

module.exports = {
  /**
   * @param body
   * @param response
   * @param window
   * @return {Array}
   */
  getCssLinks: function(body, response, window) {
    var links = [];
    var $ = window.$;
    var html = $.parseHTML(body);
    $(html)
      .each(function(num, el) {
        var href = $(el)
          .attr('href');
        if (href) {
          var csspos = href.search('.css');
          if (csspos !== -1) {
            links.push(handleLink(href, response.request));
          }
        }
      }).bind(this);

    return links
  },

  checkInputLink: function(link) {
    var output = link;
    if (output.search('http') !== 0) {
      output = 'http://' + output;
    }
    return output;
  },

  getFavicon: function(window) {
    var result = false;
    var $ = window.$;
    var links = $('link')
      .filter('[rel*="icon"]');
    if (links.length === 0) {
      result = false;
    } else if (links.length === 1) {
      result = links
        .eq(0)
        .attr('href');
    } else if (links.length > 0) {
      if (links.filter('[rel="shortcut icon"]').length > 0) {
        result = links.filter('[rel="shortcut icon"]').attr('href');
      } else if (links.filter('[type*="icon"]').length > 0) {
        result = links.filter('[type*="icon"]').attr('href');
      } else if (links.filter('href*="favicon.ico"').length > 0) {
      result = links.filter('href*="favicon.ico"').attr('href');
      } else {
        result = links
          .eq(0)
          .attr('href');
      }
    }
    return result
  },

  getStaticSrcs: function(html, inputUrl, archive) {
    XRegExp.forEach(html, config.regexp.imageSrc, function(match, i) {
      var orig = match[1];
      var filename = path.basename(orig);
      var absURL = url.resolve(inputUrl, orig);
      archive.append(request(absURL), {name: 'assets/' + filename});
    }, this);
  }
};




