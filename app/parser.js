var archiver = require('archiver');
var bcss = require('js-beautify').css;
var bhtml = require('js-beautify').html;
var cleancss = require('clean-css');
var config = require('../config/config');
var fs = require('fs');
var jade = require('jade');
var jsdom = require('jsdom');
var juice = require('juice');
var methods = require('./methods');
var Promise = require('promise');
var path = require('path');
var request = require('request');
var S = require('string');
var uncss = require('uncss');
var url = require('url');
var XRegExp = require('xregexp').XRegExp;
var _ = require('underscore');
var csso = require('csso');

var jquery = fs.readFileSync(
  path.join(__dirname, '..', 'node_modules', 'jquery', 'dist', 'jquery.js'));

/**
 * @param {Object} body A Request body
 */
module.exports = function(body) {
  return new Promise((fullfill, reject) => {
    var changedSettings = config.moduleSettings.reduce(function(ret, setting) {
      var key = setting.name;
      if (body[key] === undefined) {
        body[key] = false;
      }
      if (body[key] === 'on') {
        body[key] = true;
      }
      var edit = setting.value !== body[key];
      if (edit) {
        ret[key] = body[key];
      }
      return ret;
    }, {});

    var input = {
      slug: body.slug,
      url: methods.checkInputLink(body.url),
      header: body.header_html,
      footer: body.footer_html,
      config: changedSettings
    };

    var archive = archiver('zip');

    var moduleConfig = jade.renderFile(config.templates.config, {
      slug: input.slug,
      config: JSON.stringify(input.config, null, '\t').replace(/"/g, "'")
    });
    archive.append(moduleConfig, {name: 'config.js'});

    var moduleBase = jade.renderFile(config.templates.basejs, {
      slug: input.slug,
      name: input.slug.
          split('').
          map((char, index) => {
            if (index === 0) {
              char = char.toUpperCase();
            }
            return char;
          }).
          join(''),
      header: !!input.header,
      footer: !!input.footer,
      pretty: '  '
    });
    archive.append(moduleBase, {name: input.slug + '.js'});

    var allAssets = [];

    if (input.header) {
      var header = jade.renderFile(config.templates.headerojst, {
        html: bhtml(input.header),
        slug: input.slug
      });

      methods.getStaticSrcs(input.header, input.url, archive);
      archive.append(header, {name: 'header.ojst'});
    }

    if (input.footer) {
      var footer = jade.renderFile(config.templates.footerojst, {
        html: bhtml(input.footer),
        slug: input.slug
      });
      archive.append(footer, {name: 'footer.ojst'});
    }

    if (input.slug.length < 3) {
      input.slug = '{slug}'
    }
    request(input.url, {encoding: 'utf-8'}, function(err, response, body) {
      if (err) {
        reject(err);
      }
      if (response.statusCode === 200) {
        body = body.replace(config.regexp.iframe, '');
        jsdom.env({
          html: body,
          src: [jquery],
          done: function(err, window) {
            if (err) {
              reject(err);
            }
            try {
              var faviconUrl = methods.getFavicon(window);
            } catch (e) {
              console.error(e);
            }
            if (faviconUrl) {
              faviconUrl = url.resolve(input.url, faviconUrl)
            }

            var urls = methods.getCssLinks(body, response, window);

            var read = function(url) {
              return new Promise(function(fullfill, reject) {
                request(url, function(err, res, body) {
                  if (err) {
                    reject(err);
                  }

                  if (res.statusCode === 200) {
                    fullfill(body);
                  }
                })
              })
            };

            var readCssFiles = function(urls) {
              return Promise.all(urls.map(read));
            };

            readCssFiles(urls)
            .done(function(results) {
              var cssStrings = results;
              urls.forEach(function(link, index) {
                var assets = [];
                var absLink = link;
                var css = cssStrings[index];
                XRegExp.forEach(css, config.regexp.cssUrl, function(match, i) {
                  assets.push({
                    absURL: url.resolve(absLink, match[1]),
                    originURL: match[1]
                  });
                }, this);

                assets = _.uniq(assets, true, function(el) {
                  return el.originURL;
                });

                assets.forEach(function(asset) {
                  asset.uniqID = 'assetID' + Math.floor(Math.random() * Math.pow(10, 10));
                  asset.filename = asset.uniqID + '-' + path.basename(asset.originURL);
                  cssStrings[index] = S(cssStrings[index])
                  .replaceAll(asset.originURL, asset.uniqID).s;
                });

                allAssets.push(assets);
              });
              allAssets = _.flatten(allAssets);

              // remove unused css
              var rawHTML = input.header + (input.footer || '');
              var rawCss = cssStrings.join('');
              uncss(rawHTML, {raw: rawCss}, function(err, results) {
                if (err) {
                  console.error(err);
                }
                var output = new cleancss({keepSpecialComments: 0}).
                    minify(results).styles;
                archive.on('error', function(err) {
                  console.log('archive error!');
                  console.log(err);
                });

                // add favicon
                var favJade;
                if (faviconUrl) {
                  favJade = path.parse(faviconUrl).base;
                  archive.append(request(faviconUrl), {
                    name: 'assets/' + path.parse(faviconUrl).base
                  });
                } else {
                  favJade = 'favicon.ico';
                }

                var indexcss = jade.renderFile(config.templates.styles, {
                  slug: input.slug,
                  header: !!input.header,
                  footer: !!input.footer
                });
                archive.append(indexcss, {name: input.slug + '.scss'});

                allAssets.forEach(function(asset) {
                  var ok = config.regexp.datauri.test(asset.absURL);
                  if (output.search(asset.uniqID) !== -1 && !ok) {
                    output = S(output)
                    .replaceAll(asset.uniqID, 'img/whitelabel/' + input.slug + '/' +asset.filename).s;
                    archive.append(request(asset.absURL), {name: 'assets/' + asset.filename});
                  }
                });
                // create header.css
                uncss(input.header, {raw: output}, function(err, resultsH) {
                  var outputH = new cleancss({
                    keepSpecialComments: 0
                  }).minify(resultsH).styles;
                  var headerScssString = outputH.replace(
                      config.regexp.asseturl, function(match, p1, p2) {
                        return 'asset-url(\'' + p2 + '\')'
                      });
                  var headerSCSS = jade.renderFile(config.templates.headerscss, {
                    css: bcss(headerScssString)
                  });
                  archive.append(headerSCSS, {name: 'header.scss'});

                  if (input.footer) {
                    uncss(input.footer, {raw: output}, function(err, resultsF) {
                      var outputF = new cleancss({
                        keepSpecialComments: 0
                      }).minify(resultsF).styles;
                      var footerScssString = outputF.replace(
                          config.regexp.asseturl, function(match, p1, p2) {
                            return 'asset-url(\'' + p2 + '\')'
                          });
                      var footerSCSS = jade.renderFile(config.templates.footerscss, {
                        css: bcss(footerScssString)
                      });
                      archive.append(footerSCSS, {name: 'footer.scss'});
                      fullfill(archive);
                    })
                  } else {
                    fullfill(archive);
                  }
                })
              })
            })
          }
        })
      }
    })
  });
};
