var gulp = require('gulp');
var stylus = require('gulp-stylus');
var prefix = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var nib = require ('nib');
var spawn = require('child_process').spawn;

var node;

gulp.task('server', function() {
  if (node) {
    node.kill();
  }
  node = spawn('node', ['app/app.js'], {stdio: 'inherit'});
  node.on('close', function (code) {
    if (code === 8) {
      gulp.log('Error detected, waiting for changes...');
    }
  });
});

gulp.task('stylus', function() {
  gulp.src('styles/style.styl')
    .pipe(stylus({
      use: nib()
    }))
    .pipe(gulp.dest('public'));
});

gulp.task('default', ['stylus'], function() {
  gulp.run('server');

  gulp.watch(['./app/app.js', './app/**/*.js'], function() {
    gulp.run('server');
  });

  gulp.watch('styles/**/*.styl',['stylus']);
});

// clean up if an error goes unhandled.
process.on('exit', function() {
  if (node) {
    node.kill();
  }
});

