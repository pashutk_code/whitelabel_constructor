FROM debian:jessie
MAINTAINER Igor Kuznetsov igor.kuznetsov@ostrovok.ru

RUN set -xe && export DEBIAN_FRONTEND=noninteractive \
  && apt-get update -qq && apt-get upgrade -qq \
  && apt-get install -qq nginx-full git supervisor curl build-essential \
  && curl --fail -s https://nodejs.org/dist/v5.5.0/node-v5.5.0-linux-x64.tar.gz | tar --strip-components=1 -xz -C /usr/local \
  && npm install -g npm \
  && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV PROJECT_NAME=ostrowlom NODE_ENV=production
WORKDIR /opt/$PROJECT_NAME

COPY package.json ./
RUN npm install

COPY etc/ /etc/
COPY ./ ./

RUN nginx -t

CMD [ \
  "/usr/bin/supervisord", \
  "--nodaemon", \
  "--configuration", \
  "/etc/supervisor/supervisord.conf" \
]
