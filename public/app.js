particlesJS.load('particles-js', '/particles.json', function() {
  console.log('particles config loaded');
});

var form = document.querySelector('.form');
var submit = document.querySelector('.button-submit');

submit.addEventListener('click', function(evt) {
  evt.preventDefault();
  form.submit();
});
